﻿-- CONSULTAS DE TOTALES

  -- Número de ciclistas que hay.

SELECT COUNT(*) FROM ciclista c;

  -- Número de ciclistas que hay del equipo Banesto.

SELECT COUNT(*) FROM ciclista c
WHERE c.nomequipo='Banesto';

  -- La edad media de los ciclistas.

SELECT AVG(edad) FROM ciclista c;

  -- La edad media de los ciclistas de Banesto.

SELECT AVG(c.edad) FROM ciclista c
GROUP BY c.nomequipo='Banesto';

  -- La edad media de los ciclistas por cada equipo.

SELECT c.nomequipo, AVG(c.edad) FROM ciclista c
GROUP BY c.nomequipo;
  
  -- El nº de ciclistas por cada equipo.

SELECT nomequipo, COUNT(*) FROM ciclista c
GROUP BY c.nomequipo;

  -- El nº total de puertos.

SELECT COUNT(*) AS Nºpuertos FROM puerto p;

  -- El nº total de puertos mayores de 1500;

SELECT * FROM puerto p
WHERE p.altura > 1500;

  -- El listar el nombre de los equipos que tengan más de 4 ciclistas.

SELECT c.nomequipo, COUNT(*) nCiclistas FROM ciclista c
GROUP BY c.nomequipo
HAVING  nCiclistas> 4;

  -- Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.

SELECT c.nomequipo, COUNT(*) nCiclistas FROM ciclista c
where c.edad BETWEEN 28 AND 32
GROUP BY c.nomequipo
HAVING nCiclistas > 4 ;

  -- Nº de etapas que ha ganado cada uno de los ciclistas.

SELECT e.dorsal, COUNT(*) FROM etapa e
GROUP BY e.dorsal;

  -- Sacar el dorsal de los ciclistas que hayan ganado más de 1 etapa.

SELECT e.dorsal, COUNT(*) FROM etapa e
GROUP BY e.dorsal
HAVING COUNT(*)>1;
